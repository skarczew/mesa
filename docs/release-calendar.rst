Release Calendar
================

Overview
--------

Mesa provides feature/development and stable releases.

The table below lists the date and release manager that is expected to
do the specific release.

Regular updates will ensure that the schedule for the current and the
next two feature releases are shown in the table.

In order to keep the whole releasing team up to date with the tools
used, best practices and other details, the member in charge of the next
feature release will be in constant rotation.

The way the release schedule works is explained
:ref:`here <schedule>`.

Take a look :ref:`here <criteria>` if you'd like to
nominate a patch in the next stable release.

.. _calendar:

Calendar
--------

+--------+---------------+------------+-----------------+-----------------------------------------+
| Branch | Expected date | Release    | Release manager | Notes                                   |
+========+===============+============+=================+=========================================+
| 20.1   | 2020-10-14    | 20.1.10    | Eric Engestrom  | Last planned release of the 20.1 series |
+--------+---------------+------------+-----------------+-----------------------------------------+
| 20.2   | 2020-10-14    | 20.2.1     | Dylan Baker     |                                         |
|        +---------------+------------+-----------------+-----------------------------------------+
|        | 2020-10-28    | 20.2.2     | Dylan Baker     |                                         |
|        +---------------+------------+-----------------+-----------------------------------------+
|        | 2020-11-11    | 20.2.3     | Dylan Baker     |                                         |
|        +---------------+------------+-----------------+-----------------------------------------+
|        | 2020-11-24    | 20.2.4     | Dylan Baker     |                                         |
+--------+---------------+------------+-----------------+-----------------------------------------+
